try{
//add scripts
appendStyle('source/jquery.fancybox.css');
appendStyle('source/helpers/jquery.fancybox-buttons.css');
appendStyle('source/helpers/jquery.fancybox-thumbs.css');

//output lightbox from url
$.fancybox({
        	"href": ""+url+""
        });
        $('.fancybox').fancybox();

//append stylsheet to dom if it doesnt already exist
function appendStyle(filepath) {
	filepath = chrome.extension.getURL(filepath);
    if ($('head link[href="' + filepath + '"]').length > 0)
        return;

    var ele = document.createElement('link');
    ele.setAttribute("type", "text/css");
    ele.setAttribute("rel", "Stylesheet");
    ele.setAttribute("href", filepath);
    $('head').append(ele);
}

}catch(err){
	//lets not annoy users
}
